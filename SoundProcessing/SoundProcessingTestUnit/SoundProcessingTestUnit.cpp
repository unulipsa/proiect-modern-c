#include "pch.h"
#include "CppUnitTest.h"
#include <SFML/Graphics.hpp>
#include "../SoundProcessing/CommandLine.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SoundProcessingTestUnit
{
	TEST_CLASS(CommandLineTest)
	{
	public:
		
		TEST_METHOD(CommandParseToSingleCommTest)
		{
			std::string str1 = "-help -in -out";
			std::string str2 = "helpinout";

			Assert::AreSame(CommandLine::CommandParseToSingleComm(str1), str2);
		}
	};
}
