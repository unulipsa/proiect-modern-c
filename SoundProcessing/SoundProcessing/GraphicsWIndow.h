#pragma once
#include<SFML/Graphics.hpp>
#include<SFML/Window.hpp>


class GraphicsWIndow
{
public:

	static void create();

	virtual void draw(const sf::Drawable& drawable, const sf::RenderStates& states = sf::RenderStates::Default);

	void setSize(float x, float y);

	void setPosition(float x, float y);

	void setView(const sf::View& view);

	void resetView();

	int getFramerate();

protected:

	float m_framerate;

};

