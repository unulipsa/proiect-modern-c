#pragma once
#include "ProcessingComponent.h"
#include "AudioFile.h"
#include <string>

class CommandLine : ProcessingComponent
{
public:
	static void FullCommandToParse(AudioFile<double>& file, std::string command);
	static void SingleCommandParseToAction(AudioFile<double>& file, std::string command, float arg);

private:
};

