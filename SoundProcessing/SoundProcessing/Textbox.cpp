#include "Textbox.h"

void TextBox::inputLogic(int charTyped)
{
	if (charTyped != DELETE_KEY && charTyped != ENTER_KEY && charTyped != ESCAPE_KEY)
	{
		text << static_cast<char>(charTyped);
	}
	else if (charTyped == DELETE_KEY)
	{

		if (text.str().length() > 0) {

			deleteLastChar();
		}

	}
	textbox.setString(text.str() + "_");
}

void TextBox::deleteLastChar()
{
	std::string t = text.str();
	std::string newt = "";
	for (int i = 0; i < t.length() - 1; i++)
	{
		newt += t[i];
	}
	text.str("");
	text << newt;
	textbox.setString(text.str());
}

void TextBox::setFont(sf::Font& font)
{
	textbox.setFont(font);
}

void TextBox::draw(sf::RenderWindow& window)
{
	window.draw(textbox);
}

void TextBox::setPosition(sf::Vector2f pos)
{
	textbox.setPosition(pos);
}

void TextBox::setLimit(bool ToF)
{
	hasLimit = ToF;
}

void TextBox::setLimit(bool ToF, int lim)
{
	hasLimit = ToF;
	limit = lim;
}

void TextBox::setSelected(bool sel)
{
	isSelected = sel;
	if (!sel)
	{
		std::string t = text.str();
		std::string newt = "";
		for (int i = 0; i < t.length() - 1; i++)
		{
			newt += t[i];
		}
		textbox.setString(newt);
	}
}

std::string TextBox::getText()
{
	return text.str();
}

void TextBox::typedOn(sf::Event input)
{
	if (isSelected)
	{
		int charTyped = input.text.unicode;
		if (charTyped < 120)
		{
			if (hasLimit)
			{
				if (text.str().length() <= limit)
				{
					inputLogic(charTyped);
				}
				else if (text.str().length() > limit && charTyped == DELETE_KEY)
				{
					deleteLastChar();
				}
			}
			else
			{
				inputLogic(charTyped);
			}
		}
	}
}

