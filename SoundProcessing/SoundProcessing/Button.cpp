#include "Button.h"
#include"FileManipulations.h"
#include"Slider.h"
void Button::setFont(sf::Font& font)
{
	text.setFont(font);
}

void Button::setBackColor(sf::Color color)
{
	button.setFillColor(color);
}

void Button::setTextColor(sf::Color color)
{
	text.setColor(color);
}

void Button::setPosition(sf::Vector2f pos)
{
	button.setPosition(pos);

	float xPos = (pos.x + button.getGlobalBounds().width / 2) - (text.getGlobalBounds().width / 2);
	float yPos = (pos.x + button.getGlobalBounds().height / 2) - (text.getGlobalBounds().height / 2);

	text.setPosition({ xPos, yPos });
}

void Button::drawTo(sf::RenderWindow& window)
{
	window.draw(button);
	window.draw(text);
}

bool Button::isMouseOver(sf::RenderWindow& window)
{
	float mouseX = sf::Mouse::getPosition(window).x;
	float mouseY = sf::Mouse::getPosition(window).y;

	float btnPosX = button.getPosition().x;
	float btnPosY = button.getPosition().y;

	float btnxPosWidth = button.getPosition().x + button.getLocalBounds().width;
	float btnyPosHeight = button.getPosition().y + button.getLocalBounds().height;

	if (mouseX < btnxPosWidth && mouseX > btnPosX && mouseY < btnyPosHeight && mouseY > btnPosY) {

		return true;
	}

	return false;
}

void Button::save(AudioFile<double> file, std::string path)
{
	FileManipulations::SoundFileWriter(file, path);
}

void Button::load(AudioFile<double> file, std::string path)
{
	FileManipulations::SoundFileReader(file, path);
}

void Button::openEditor()
{
		sf::RenderWindow window;
		SliderSFML scaleup(100, 25);
		SliderSFML scaledown(100, 75);
		SliderSFML pitchup(100, 125);
		SliderSFML pitchdown(100, 175);
		scaleup.create(1, 10);
		scaledown.create(1, 10);
		pitchup.create(1, 10);
		pitchdown.create(1, 10);
		window.create(sf::VideoMode(700, 568), "Editor", sf::Style::Titlebar | sf::Style::Close);
		Button apply("Apply", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
		apply.setPosition({ 50, 350 });
		Button cancel("Cancel", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
		cancel.setPosition({ 160, 350 });
		while (window.isOpen())
		{
			sf::Event Event;
			while (window.pollEvent(Event))
			{
				switch (Event.type)
				{

				case sf::Event::Closed:

					window.close();
				}
			}
			window.clear(sf::Color::Green);
			scaleup.returnText(1, 5, "test", 14);
			scaleup.draw(window);
			scaledown.returnText(1, 5, "tet", 14);
			scaledown.draw(window);
			pitchup.returnText(1, 5, "test", 14);
			pitchup.draw(window);
			pitchdown.returnText(1, 5, "test", 14);
			pitchdown.draw(window);
			apply.drawTo(window);
			cancel.drawTo(window);
			apply.update({50,350});
			cancel.update({ 160,350 });
			window.display();
		}
		
}

void Button::update(const sf::Vector2f posmouse)
{
	buttonstate = btn_idle;
	if (button.getGlobalBounds().contains(posmouse))
	{
		buttonstate = btn_haver;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			buttonstate = btn_pressed;
		}
	}
	switch (buttonstate)
	{
	case btn_idle:
		button.setFillColor(sf::Color::Cyan);
		break;
	case btn_haver:
		button.setFillColor(sf::Color::Yellow);
		break;
	case btn_pressed:
		button.setFillColor(sf::Color::Magenta);
		break;
	default:
		button.setFillColor(sf::Color::Red);
		break;
	}
}

const bool Button::isPressed() const
{
	if (buttonstate == btn_pressed)
		return true;
	return false;
}

