#include <string>
#include <vector>
#include "pch.h"
#include "CppUnitTest.h"
#include "../SoundProcessing/ConfigurationFile.h"
#include "../SoundProcessing/AudioFile.h"
#include "../SoundProcessing/FileManipulations.h"
#include "../SoundProcessing/ProcessingComponent.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SoundProcessingTest
{
	TEST_CLASS(SoundProcessingTest)
	{
	public:
		TEST_METHOD(ConfigFileParseSize)
		{
			ConfigurationFile file;
			file.ParseConfigFile("configtest.txt");
			int actual = file.GetConfigSize();

			Assert::AreEqual(3, actual);
		}
		
		TEST_METHOD(ConfigFileParseCommands)
		{
			ConfigurationFile file;
			file.ParseConfigFile("configtest.txt");
			std::string actual;

			for (uint16_t i = 0; i < file.GetConfigSize(); i++)
				actual += file.GetConfigElement(i);

			Assert::AreEqual(std::string("scaledown2"), actual);
		}

		TEST_METHOD(PitchUpTest)
		{
			AudioFile<double> buffer;
			buffer.load("test-audio.wav");

			float initialRate = buffer.getSampleRate();
			ProcessingComponent::PitchUp(buffer, 2);
			float finalRate = buffer.getSampleRate();

			Assert::AreEqual(initialRate * 2, finalRate);
		}

		TEST_METHOD(PitchDownTest)
		{
			AudioFile<double> buffer;
			buffer.load("test-audio.wav");

			float initialRate = buffer.getSampleRate();
			ProcessingComponent::PitchDown(buffer, 2);
			float finalRate = buffer.getSampleRate();

			Assert::AreEqual(initialRate / 2, finalRate);
		}

		TEST_METHOD(ScaleUpTest)
		{
			AudioFile<double> buffer;
			buffer.load("test-audio.wav");

			float initialSample = buffer.samples[0][1];
			ProcessingComponent::ScaleUp(buffer, 2);
			float finalSample = buffer.samples[0][1];

			Assert::AreEqual(initialSample * 2, finalSample);
		}

		TEST_METHOD(ScaleDownTest)
		{
			AudioFile<double> buffer;
			buffer.load("test-audio.wav");

			float initialSample = buffer.samples[0][1];
			ProcessingComponent::ScaleDown(buffer, 2);
			float finalSample = buffer.samples[0][1];

			Assert::AreEqual(initialSample / 2, finalSample);
		}
	};
}
