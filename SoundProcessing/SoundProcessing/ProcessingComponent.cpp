#include "ProcessingComponent.h"
#include <SFML/Audio.hpp>
#include <iostream>
#include <vector>

void ProcessingComponent::AccessSamples(AudioFile<double> file)
{
	int numSamples = file.getNumSamplesPerChannel();
	for (int i = 0; i < numSamples; i++)
	{
		double currentSample = file.samples[0][i];
	}
}

void ProcessingComponent::ScaleUp(AudioFile<double>& file, float percent)
{
	double initialSample, currentSample;
	bool check = true;

	for (int index1 = 0; index1 < file.getNumChannels(); index1++)
	{
		for (int index2 = 0; index2 < file.getNumSamplesPerChannel(); index2++)
		{
			initialSample = file.samples[index1][index2];
			file.samples[index1][index2] *= percent;
			currentSample = file.samples[index1][index2];

			if (currentSample != (double)(initialSample * percent))
			{
				check = false;
				break;
			}
		}

		if (check == false)
			break;
	}

	if (check == true)
		std::cout << "Command 'scaleup' executed succesfully.\n";
	else
		std::cout << "Command 'scaleup' failed;\n";
}

void ProcessingComponent::ScaleDown(AudioFile<double>& file, float percent)
{
	double initialSample, currentSample;
	bool check = true;
	for (int index1 = 0; index1 < file.getNumChannels(); index1++)
	{
		for (int index2 = 0; index2 < file.getNumSamplesPerChannel(); index2++)
		{
			initialSample = file.samples[index1][index2];
			file.samples[index1][index2] /= percent;
			currentSample = file.samples[index1][index2];

			if (currentSample != (double)(initialSample / percent))
			{
				check = false;
				break;
			}
		}

		if (check == false)
			break;
	}

	if (check == true)
		std::cout << "Command 'scaledown' executed succesfully.\n";
	else
		std::cout << "Command 'scaledown' failed;\n";
}

void ProcessingComponent::PlaybackSpeedUp(AudioFile<double>& file, float percent)
{
	uint32_t initialSampleRate = file.getSampleRate();
	file.setSampleRate(file.getSampleRate() * percent);
	uint32_t currentSampleRate = file.getSampleRate();

	if (currentSampleRate == initialSampleRate * percent)
		std::cout << "Command 'speedup' executed succesfully.\n";
	else
		std::cout << "Command 'speedup' failed.\n";
}

void ProcessingComponent::PlaybackSpeedDown(AudioFile<double>& file, float percent)
{
	uint32_t initialSampleRate = file.getSampleRate();
	file.setSampleRate(file.getSampleRate() / percent);
	uint32_t currentSampleRate = file.getSampleRate();

	if (currentSampleRate == initialSampleRate / percent)
		std::cout << "Command 'speeddown' executed succesfully.\n";
	else
		std::cout << "Command 'speeddown' failed.\n";
}

void ProcessingComponent::PlaySound(AudioFile <double>& file)
{
	sf::SoundBuffer buffer;
	sf::Sound sound;
	std::vector<std::vector<sf::Int16>> samples;
	std::vector<sf::Int16> samplesPerChannel;


	for (int index = 0; index < file.getNumChannels(); index++)
	{
		samples.push_back(samplesPerChannel);
		for (int index2 = 0; index2 < file.samples.size(); index2++)
			samples[index].push_back((sf::Int16)file.samples[index][index2]);
	}

	buffer.loadFromSamples(&samples[0][0], file.samples.size(), file.getNumChannels(), file.getNumSamplesPerChannel());
	sound.setBuffer(buffer);
	std::cout << "Sound currently playing...\n";
	sound.play();
}

void ProcessingComponent::ShowFileInfo(AudioFile<double> file)
{
	std::cout << "Showing information for the loaded audio file: " << '\n';
	std::cout << "Bit depth: " << file.getBitDepth() << '\n';
	std::cout << "Length (seconds): " << file.getLengthInSeconds() << '\n';
	std::cout << "Number of channels: " << file.getNumChannels() << '\n';
	std::cout << "Number of samples (per channel): " << file.getNumSamplesPerChannel() << '\n';
	std::cout << "Sample rate (samples per second): " << file.getSampleRate() << '\n';
}