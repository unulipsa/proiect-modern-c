#include <SFML/Graphics.hpp>
#include <SFML/Audio/Sound.hpp>
#include <iostream>
#include <string>
#include "CommandLine.h"
#include "AudioFile.h"
#include "SinWave.h"
#include "ConfigurationFile.h"
#include "GraphicsWIndow.h"
#include "FileManipulations.h"

int main()
{
	std::string fullCommand;
	AudioFile<double> file;
	std::string status;
	float arg = -1;
	int choice = 0;

	std::cout << "Choose what method do you want to use for input:\n";
	std::cout << "1. Keyboard input.\n";
	std::cout << "2. Configuration file.\n";
	std::cout << "3. Record audio.\n";
	std::cout << "4. Graphics.\n";
	std::cin >> choice;
	
	switch (choice)
	{
	case 1:
	{
		std::cout << "Welocome! Please type your command for sound file processing (type --help for the list of commands) \n";
		
		CommandLine::FullCommandToParse(file, fullCommand);
		break;
	}
	case 2:
	{
		ConfigurationFile configFile;

		std::string path;
		uint16_t index = 0;

		std::cout << "Choose a location to load a  configuration file: " << std::endl;
		std::cin.ignore();
		std::getline(std::cin, path);

		configFile.ParseConfigFile(path);
		configFile.ParseConfigElementToAction(file);

		break;
	}
	case 3:
	{
		FileManipulations::AudioRecorder();
		
		std::cout << "If the audio recorder is not supported, please chose another input method.\n";
		std::cout << "Welocome! Please type your command for sound file processing (type -help for the list of commands).\n";
		std::cout << "You can exclude the --filein, --fileout and --record options as you already have an audio file to work with.\n";
		CommandLine::FullCommandToParse(file, fullCommand);
	}
	case 4:
	{
		//GraphicsWIndow::create();
		break;
	}
	default: 
		break;
	}

	return 0;
}