#pragma once
#include"AudioFile.h"
class FileManipulations
{
public:
	static void SoundFileReader(AudioFile<double>& buffer);
	static void SoundFileReader(AudioFile<double>& buffer, std::string path);
	static void SoundFileWriter(AudioFile<double>& buffer);
	static void SoundFileWriter(AudioFile<double>& buffer, std::string path);
	static void AudioRecorder();

private:
	
};

