#include <algorithm>
#include <string>
#include <vector>
#include "ConfigurationFile.h"
#include "ProcessingComponent.h"
#include "FileManipulations.h"
#include "ProcessingComponent.h"

void ConfigurationFile::ParseConfigFile(std::string filePath)
{
    std::ifstream configFile(filePath);
    if (configFile.is_open())
    {
        while (getline(configFile, m_line))
        {
            m_line.erase(std::remove_if(m_line.begin(), m_line.end(), isspace),
                m_line.end());
            if (m_line.empty() || m_line[0] == '#')
            {
                continue;
            }

            uint16_t delimiterPosition = m_line.find("=");

            m_config.push_back(m_line.substr(delimiterPosition + 1));
        }
    }
    else
    {
        std::cerr << "Couldn't open config file for reading.\n";
    }
    configFile.close();
}

void ConfigurationFile::ParseConfigElementToAction(AudioFile<double>& buffer)
{
    uint16_t index = 2;

    FileManipulations::SoundFileReader(buffer, m_config[0]);
    std::string outputPath = m_config[1];

    while (index < m_config.size())
    {
        if (m_config[index] == "scale")
            if (m_config[index + 1] == "up")
            {
                std::cout << "Command 'scaleup' in execution:\n";
                ProcessingComponent::ScaleUp(buffer, stof(m_config[index + 2]));
            }
            else if (m_config[index + 1] == "down")
            {
                std::cout << "Command 'scaledown' in execution:\n";
                ProcessingComponent::ScaleDown(buffer, stof(m_config[index + 2]));
            }
            else std::cout << "Invalid type of scaling in configure file (only 'up' and 'down' allowed).\n";

        else if (m_config[index] == "speed")
            if (m_config[index + 1] == "up")
            {
                std::cout << "Command 'speedup' in execution:\n";
                ProcessingComponent::PlaybackSpeedUp(buffer, stof(m_config[index + 2]));
            }
            else if (m_config[index + 1] == "down")
            {
                std::cout << "Command 'speeddown' in execution:\n";
                ProcessingComponent::PlaybackSpeedDown(buffer, stof(m_config[index + 2]));
            }

            else std::cout << "Invalid type of scaling in configure file (only 'up' and 'down' allowed).\n";

        else std::cout << "Invalid operation for sound processing in configure file.\n";

        index += 3;
    }
        FileManipulations::SoundFileWriter(buffer, outputPath);
}