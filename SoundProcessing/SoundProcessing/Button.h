#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
#include "AudioFile.h"
enum button_states{btn_idle=0, btn_haver, btn_pressed};
class Button
{
public:
	short unsigned buttonstate;
	Button() {}

	Button(const std::string t, sf::Vector2f size, int charSize, sf::Color bgColor, sf::Color textColor)
	{
		//text.setString(t);
		text.setColor(textColor);
		text.setCharacterSize(charSize);
		buttonstate = btn_idle;
		button.setSize(size);
		button.setFillColor(bgColor);

	}

	void setFont(sf::Font& font);
	void setBackColor(sf::Color color);
	void setTextColor(sf::Color color);
	void setPosition(sf::Vector2f pos);
	void drawTo(sf::RenderWindow& window);

	bool isMouseOver(sf::RenderWindow& window);
	void save(AudioFile<double> file, std::string path);
	void load(AudioFile<double> file, std::string path);
	void openEditor();
	void update(const sf::Vector2f posmouse);
	const bool isPressed() const;

private:

	sf::RectangleShape button;
	sf::Text text;

};

