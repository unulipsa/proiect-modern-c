#pragma once
#include "AudioFile.h"

class ProcessingComponent
{
	public:
	static void ScaleUp(AudioFile<double>& file, float percent);
	static void ScaleDown(AudioFile<double>& file, float percent);
	static void PlaybackSpeedUp(AudioFile<double>& file, float percent);
	static void PlaybackSpeedDown(AudioFile<double>& file, float percent);
	static void PlaySound(AudioFile <double>& file);
	static void ShowFileInfo(AudioFile<double> file);
	void AccessSamples(AudioFile<double> file);
};