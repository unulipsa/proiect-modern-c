#pragma once
#include <string>
#include <vector>
#include "AudioFile.h"

class ConfigurationFile
{
public:
    void ParseConfigFile(std::string filePath);
    void ParseConfigElementToAction(AudioFile<double>& buffer);

private:
    std::string m_line;
    std::vector<std::string> m_config;
};