#pragma once
#include <iostream>
#include "SFML/Audio/SoundSource.hpp"
#include "SFML/Audio/SoundBuffer.hpp"
#include "SFML/Audio/Sound.hpp"
#include "AudioFile.h"
#include <math.h>
#include <vector>

class SinWave
{
public:

	short static SineWave(double time, double freq, double amp);

};

