#include "GraphicsWIndow.h"
#include"Button.h"
#include"FileManipulations.h"
void GraphicsWIndow::create()
{

	sf::RenderWindow window;

	sf::Vector2i centerWindow((sf::VideoMode::getDesktopMode().width / 2) - 445, (sf::VideoMode::getDesktopMode().height / 2) - 480);

	window.create(sf::VideoMode(700, 568), "Audio Manipulator & Wave Generator", sf::Style::Titlebar | sf::Style::Close);
	//window.setPosition(centerWindow);

	window.setKeyRepeatEnabled(true);
	Button save("Save", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
	save.setPosition({ 50, 350 });
	Button load("Load", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
	load.setPosition({ 160, 350 });
	Button edit("Edit", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
	edit.setPosition({ 270, 350 });
	Button apply("Apply", { 100, 50 }, 30, sf::Color::Blue, sf::Color::Black);
	apply.setPosition({ 380, 350 });
	while (window.isOpen()) {

		sf::Event Event;

		while (window.pollEvent(Event))
		{

			switch (Event.type)
			{

			case sf::Event::Closed:

				window.close();
				break;
			case sf::Event::MouseButtonPressed:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					edit.openEditor();
				}
			case sf::Event::MouseButtonPressed:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					save.save(AudioFile<double> file, "");
				}
			case sf::Event::MouseButtonPressed:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					load.load(AudioFile<double> file, "");
				}
			}
			 
		}

		window.clear(sf::Color::Green);
		save.drawTo(window);
		load.drawTo(window);
		edit.drawTo(window);
		apply.drawTo(window);
		window.display();

		}

	}

void GraphicsWIndow::draw(const sf::Drawable& drawable, const sf::RenderStates& states)
{
	sf::RenderWindow::draw(drawable, states);
}

void GraphicsWIndow::setSize(float x, float y)
{
	sf::RenderWindow::setSize(sf::Vector2u(x, y));
}

void GraphicsWIndow::setPosition(float x, float y)
{
	sf::RenderWindow::setPosition(sf::Vector2i(x, y));
}

void GraphicsWIndow::setView(const sf::View& view)
{
	sf::RenderWindow::setView(view);
}

void GraphicsWIndow::resetView()
{
	setView(sf::RenderWindow::getDefaultView());
}

int GraphicsWIndow::getFramerate()
{
	return m_framerate;
}
