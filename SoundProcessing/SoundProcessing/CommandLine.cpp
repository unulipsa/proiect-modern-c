#include "CommandLine.h"
#include "FileManipulations.h"
#include "ProcessingComponent.h"
#include "AudioFile.h"
#include <string>
#include <iostream>
#include"SFML/Audio.hpp"

void CommandLine::FullCommandToParse(AudioFile<double>& file, std::string command)
{
	float args = 0;
	int i = 0;
	std::string word, fullcommand, stringArgs;
	
	while (std::getline(std::cin, command))
	{
		word += command;
		//std::cout << word;
		i = 0;
		if (word.substr(0, 2) == "--")
		{
			i = 2;
			while (i < word.size())
			{
				if (word[i] >= 'a' && word[i] <= 'z')
				{
					fullcommand += word[i];
				}
				else if (word[i] == '.' || (word[i] >= '0' && word[i] <= '9'))
				{
					stringArgs += word[i];
				}
				i++;
			}
		}
		else if (word[0] != NULL)
			std::cout << "Wrong text format. Commands should begin with '--', type '--help' for more information.\n";

		if (stringArgs != "")
			args = std::stof(stringArgs);

		if (fullcommand.size() > 0)
		{
			CommandLine::SingleCommandParseToAction(file, fullcommand, args);
			std::cout << std::endl;
			std::cout << "Choose other option to process the sound file:" << std::endl;
			fullcommand.clear();
			args = 0;
		}

		word.clear();
		fullcommand.clear();
		args = 0;
	}
}

void CommandLine::SingleCommandParseToAction(AudioFile<double>& file, std::string command, float arg)
{
	if (command == "help")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		std::cout << "List of available commands:" << std::endl;
		std::cout << "--filein | Loads an audio file from disk into the buffer." << std::endl;
		std::cout << "--fileout | Saves the final audio file on disk." << std::endl;
		std::cout << "--record | Allows you to record an audio file on the spot and save it to disk for further use." << std::endl;
		std::cout << "--scaleup (nr of times) | Increases the audio volume by the amount of times specified." << std::endl;
		std::cout << "--scaledown (nr of times) | Decreases the audio volume by the amount of times specified." << std::endl;
		std::cout << "--speedup (nr of times) | Increases the audio playback speed by the amount of times specified." << std::endl;
		std::cout << "--speeddown (nr of times) | Decreases the audio playback speed by the amount of times specified." << std::endl;
		std::cout << "--info | Shows information about the loaded audio file (file must be loaded first)." << std::endl;
		std::cout << "--play | Plays the current loaded audio file." << std::endl;
		std::cout << "Command '" << command << "' executed succesfully." << std::endl;
	}
	else if (command == "filein")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		FileManipulations::SoundFileReader(file);
	}
	else if (command == "fileout")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		FileManipulations::SoundFileWriter(file);
	}
	else if (command == "record")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		FileManipulations::AudioRecorder();
	}
	else if (command == "scaleup" && arg)
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::ScaleUp(file, arg);
	}
	else if (command == "scaledown" && arg)
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::ScaleDown(file, arg);
	}
	else if (command == "speedup" && arg)
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::PlaybackSpeedUp(file, arg);
	}
	else if (command == "speeddown" && arg)
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::PlaybackSpeedDown(file, arg);
	}
	else if (command == "info")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::ShowFileInfo(file);
		std::cout << "Command '" << command << "' executed succesfully." << std::endl;
	}
	else if (command == "play")
	{
		std::cout << "Command '" << command << "' in execution:" << std::endl;
		ProcessingComponent::PlaySound(file);
		std::cout << "Command '" << command << "' executed succesfully." << std::endl;
	}
	else
	{
		std::cout << "This command does not exist, try other command or type '--help' to see the list of available commands.";
	}
}

