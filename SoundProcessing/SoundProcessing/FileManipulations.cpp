#include "FileManipulations.h"
#include <SFML/Audio.hpp>
#include <string>

void FileManipulations::SoundFileReader(AudioFile<double> &buffer)
{
	std::string path;
	std::cout << "Choose a location to load a file: " << std::endl;
	std::getline(std::cin, path);

	if (buffer.load(path) == 0)
	{
		std::cout << "Can`t find the file. \n";
	}
	else
	{
		std::cout << "File loaded successfully. \n";
	}
}

void FileManipulations::SoundFileReader(AudioFile<double> &buffer, std::string path)
{
	if (buffer.load(path) == 0)
	{
		std::cout << "Can`t find the file. \n";
	}
	else
	{
		std::cout << "File loaded successfully. \n";
	}
}

void FileManipulations::SoundFileWriter(AudioFile<double> &buffer)
{
	std::string path;
	std::cout << "Choose a location to save: " << std::endl;
	std::getline(std::cin, path);

	if (buffer.save(path, AudioFileFormat::Wave) == 0)
	{
		std::cout << "File was not saved. \n";
	}
	else
	{
		std::cout << "File was saved with success. \n";
	}
}

void FileManipulations::SoundFileWriter(AudioFile<double> &buffer, std::string path)
{
	if (buffer.save(path, AudioFileFormat::Wave) == 0)
	{
		std::cout << "File was not saved. \n";
	}
	else
	{
		std::cout << "File was saved with success. \n";
	}
}

void FileManipulations::AudioRecorder()
{
	if (sf::SoundRecorder::isAvailable)
	{
		std::cout << "Audio recording is available on this device.\n";

		std::string action;
		std::vector<std::string> availableDevices = sf::SoundRecorder::getAvailableDevices();
		std::string inputDevice = availableDevices[0];
		sf::SoundBufferRecorder recorder;

		std::cout << "Press the 'Enter' key to start recording.\n";
		std::cin >> action;
		recorder.start();
		std::cout << "Recording started...\n";

		std::cout << "Press the 'Enter' key again to stop recording.\n";
		std::cin >> action;
		recorder.stop();

		const sf::SoundBuffer& buffer = recorder.getBuffer();

		if (buffer.saveToFile("recording.wav"))
			std::cout << "Recording was saved successfully.\n";
		else
			std::cout << "Recording was not saved.\n";
	}
	else
		std::cout << "Audio recording is not available on this device.\n";
}
