#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>
#include <sstream>

#define DELETE_KEY 8
#define ENTER_KEY 13
#define ESCAPE_KEY 27

class TextBox {

public:
	TextBox() { };

	TextBox(int size, sf::Color color, bool sel) 
	{

		textbox.setCharacterSize(size);
		textbox.setColor(color);
		isSelected = sel;
		if (sel)
		{
			textbox.setString("_");
		}
		else
		{
			textbox.setString("");
		}
	}
	void draw(sf::RenderWindow& window);
	void inputLogic(int charTyped);
	void deleteLastChar();
	void setFont(sf::Font& font);
	void setPosition(sf::Vector2f pos);
	void setLimit(bool ToF);
	void setLimit(bool ToF, int lim);
	void setSelected(bool sel);
	std::string getText();
	void typedOn(sf::Event input);
private:

	sf::Text textbox;
	std::ostringstream text;
	bool isSelected = false;
	bool hasLimit = false;
	int limit;
};

