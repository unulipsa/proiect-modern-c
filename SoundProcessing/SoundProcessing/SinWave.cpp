#include "SinWave.h"

# define PI 3.14159265358979323846

short SinWave::SineWave(double time, double freq, double amp)
{
	short sound;
	double tpc = 44100 / freq;
	double cycles = time / tpc;
	double rad = 2 * PI * cycles;
	short amplitude = 32767 * amp;
	sound = amplitude * sin(rad);

	return sound;
}